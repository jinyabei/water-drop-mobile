import { useRef } from "react"

/**
 * 获取最新的value
 * @param value 
 */
const useLatest = <T>(value:T)=>{
  //缓存
  const ref = useRef(value)
  ref.current = value;
  return ref;
}

export default useLatest